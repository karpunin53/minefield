/*
 * main.c
 * Copyright (C) 2022 Alexander Karpunin <karpunin53@vk.com>
 *
 * Minefield is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Minefield is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>

#include "ui.h"

#include "bomb.xpm"
#include "flag.xpm"
#include "win.xpm"
#include "die.xpm"
#include "smile.xpm"
#include "close.xpm"
#include "bigFlag.xpm"

#if defined(_WIN32)
    #include <Windows.h>
#endif

int field[8][8] =  {{ 0, 0, 0, 0, 0, 0, 0, 0},
                    { 0, 0, 0, 0, 0, 0, 0, 0},
                    { 0, 0, 0, 0, 0, 0, 0, 0},
                    { 0, 0, 0, 0, 0, 0, 0, 0},
                    { 0, 0, 0, 0, 0, 0, 0, 0},
                    { 0, 0, 0, 0, 0, 0, 0, 0},
                    { 0, 0, 0, 0, 0, 0, 0, 0},
                    { 0, 0, 0, 0, 0, 0, 0, 0}};

int face[8][8] =   {{ 0, 0, 0, 0, 0, 0, 0, 0},
                    { 0, 0, 0, 0, 0, 0, 0, 0},
                    { 0, 0, 0, 0, 0, 0, 0, 0},
                    { 0, 0, 0, 0, 0, 0, 0, 0},
                    { 0, 0, 0, 0, 0, 0, 0, 0},
                    { 0, 0, 0, 0, 0, 0, 0, 0},
                    { 0, 0, 0, 0, 0, 0, 0, 0},
                    { 0, 0, 0, 0, 0, 0, 0, 0}};

int totalBombs = 10, score = 0;
gboolean startBit = FALSE, timerStop = FALSE;
GtkBuilder *builder;
GError *err = NULL;
GtkWidget *mbtn, *aboutDialog, *timerLabel;
GdkPixbuf *xpmFlag, *xpmWin, *xpmDie, *xpmBomb,
          *xpmSmile, *xpmClose, *xpmBigFlag;
GtkImage *flag,  *win, *die, *bomb, *smile, *cloze, *bigFlag;

char btnCSSplusColor[36],
     *btnCSS = "* { border-bottom: 1px solid #808080; \
                    border-right: 1px solid #808080; \
                    border-top: 0px; \
                    border-left: 0px; \
                    background: #c0c0c0; \
                    font-family: monospace; \
                    font-weight: bold; \
                    font-size: 18px; ";

int intSort(const void *a, const void *b) {
    return *((int*)a) > *((int*)b);
}

void bSort(void *a, size_t item, size_t size, 
           int (*cmp)(const void*, const void*)) {
    size_t i;
    void *tmp = NULL;
    void *prev, *cur;
    gboolean F;

    tmp = malloc(item);

    do {
        F = FALSE;
        i = 1;
        prev = (char*)a;
        cur  = (char*)prev + item;
        while (i < size) {
            if (cmp(cur, prev)) {
                memcpy(tmp, prev, item);
                memcpy(prev, cur, item);
                memcpy(cur, tmp, item);
                F = TRUE;
            }
            i++;
            prev = (char*)prev + item;
            cur  = (char*)cur  + item;
        }
    } while (F);

    free(tmp);
}

int arrayCheck(int *arr, int sizeArr){
    int i = 0, result = 0;
    for (i = 0; i < sizeArr; i++){
        if(arr[i] == arr[i+1]){
            result = 0;
            break;
        } else {
            result = 1;
        }
    }
    return result;
}

gboolean TimerFunc(gpointer data) {
    static int counter=1;
    gchar *str = g_strdup_printf("%003d",counter%999);
    gtk_label_set_text(GTK_LABEL(data), str);
    g_free(str);
    counter++;
    if ( counter <= 999 && timerStop == FALSE )
        return TRUE;
    else 
        return FALSE;
}

G_MODULE_EXPORT
void quit(){
    exit(0);
}

G_MODULE_EXPORT
void restart(){
    const char *programName;
    programName = g_get_prgname();
#if defined(_WIN32)
    STARTUPINFO cif;
    ZeroMemory(&cif,sizeof(STARTUPINFO));
    PROCESS_INFORMATION pi;
    CreateProcess(programName,NULL,NULL,NULL,FALSE,NULL,NULL,NULL,&cif,&pi);
#elif defined(__linux__)
    execl(programName, programName, "", "255", NULL);
#endif
    exit(0);
}

G_MODULE_EXPORT
void about(){
    aboutDialog = GTK_WIDGET( gtk_builder_get_object (builder, "aboutDialog") );
    bigFlag = (void *)GTK_WIDGET( gtk_builder_get_object (builder, "bigFlag") );
    xpmBigFlag = gdk_pixbuf_new_from_xpm_data((void *)bigFlag_xpm);
    gtk_image_set_from_pixbuf(bigFlag, xpmBigFlag);

    gtk_dialog_run( GTK_DIALOG(aboutDialog) );
    gtk_widget_hide( GTK_WIDGET(aboutDialog) );
}

G_MODULE_EXPORT 
void Finish( GtkWidget *widget, GdkEventButton *event, gpointer data ) {
    gtk_widget_set_sensitive(data, FALSE);
}

G_MODULE_EXPORT 
void click( GtkWidget *widget, GdkEventButton *event, gpointer data ) {

    char name[2], val[2], scoreCh0[3]="0", scoreCh1[3]="0";
    sprintf(name, "%s", gtk_widget_get_name(widget));

    int i = (int)name[0] - 48;
    int j = (int)name[1] - 48;

    sprintf(val, "%d", field[i][j]);

    GtkCssProvider *provider = gtk_css_provider_new();

    if (event->type == GDK_BUTTON_PRESS && event->button == 3){

        if( gtk_builder_add_from_string (builder, ui_xml, -1, &err) == 0 ) {
            printf("Error adding build from file. Error: %s\n", err->message);
        }

        xpmFlag = gdk_pixbuf_new_from_xpm_data((void *)flag_xpm);
        flag = (void *)gtk_image_new();
        gtk_image_set_from_pixbuf(flag, xpmFlag);
        gtk_button_set_image(GTK_BUTTON(widget),(void *) flag);

        gtk_widget_set_visible(GTK_WIDGET(flag), FALSE);
        gtk_widget_set_visible(GTK_WIDGET(widget), FALSE);
        gtk_css_provider_load_from_data (provider, "* { color: #ff0000; \
                                                        font-size: 18px; \
                                                        font-weight: normal; }", 
                                         -1, NULL);

        if(face[i][j] != 1  && score < totalBombs){
            face[i][j] = 1;
            gtk_widget_set_visible(GTK_WIDGET(flag), TRUE);
            score++;
        } else if(face[i][j] == 1){
            face[i][j] = 0;
            gtk_widget_set_visible(GTK_WIDGET(flag), FALSE);
            score--;
        }

        sprintf(scoreCh0, "%d", totalBombs-score);

        if(totalBombs-score == totalBombs)
            sprintf(scoreCh1, "0%s", scoreCh0);
        else
            sprintf(scoreCh1, "00%s", scoreCh0);

        gtk_label_set_text(data, scoreCh1);
        gtk_widget_set_visible(widget, TRUE);

        if(field[i][j] == -1 && face[i][j] == 1){
            if(score == totalBombs){
	      
		if( gtk_builder_add_from_string (builder,
						 ui_xml, -1, &err) == 0 ) {
		    printf("Error adding build from file. Error: %s\n",
			   err->message);
                }

                xpmWin = gdk_pixbuf_new_from_xpm_data((void *)win_xpm);
                win = (void *)gtk_image_new();
                gtk_image_set_from_pixbuf(win, xpmWin);
                gtk_button_set_image(GTK_BUTTON(mbtn), (void *)win);

                timerStop = TRUE;
            }
        }
    }

    if (event->type == GDK_BUTTON_PRESS && 
        event->button == 1 &&
        face[i][j] != 1) {

        switch( field[i][j] ){
            case -1:
                sprintf(btnCSSplusColor, "%s%s", btnCSS, "color: #ff0000; }");
                gtk_css_provider_load_from_data (provider, btnCSSplusColor, 
                                                 -1, NULL);

                if( gtk_builder_add_from_string (builder,
						 ui_xml, -1, &err) == 0 ) {
		    printf("Error adding build from file. Error: %s\n",
			   err->message);
		}

                xpmBomb = gdk_pixbuf_new_from_xpm_data((void *)bomb_xpm);
                bomb = (void *)gtk_image_new();
                gtk_image_set_from_pixbuf(bomb, xpmBomb);

                gtk_button_set_image(GTK_BUTTON(widget), (void *)bomb);

                xpmDie = gdk_pixbuf_new_from_xpm_data((void *)die_xpm);
                die = (void *)gtk_image_new();
                gtk_image_set_from_pixbuf(die, xpmDie);
                gtk_button_set_image(GTK_BUTTON(mbtn), (void *)die);

                timerStop = TRUE;
                break;

            case 0:
                if(startBit == FALSE){
                    g_timeout_add_seconds(1, TimerFunc, timerLabel);
                    startBit = TRUE;
                }
                gtk_button_set_label(GTK_BUTTON(widget), "");
                sprintf(btnCSSplusColor, "%s%s", btnCSS, "color: #0000ff; }");
                gtk_css_provider_load_from_data (provider, btnCSSplusColor, 
                                                 -1, NULL);
                break;

            case 1:
                if(startBit == FALSE){
                    g_timeout_add_seconds(1, TimerFunc, timerLabel);
                    startBit = TRUE;
                }
                gtk_button_set_label(GTK_BUTTON(widget), val);
                sprintf(btnCSSplusColor, "%s%s", btnCSS, "color: #0000ff; }");
                gtk_css_provider_load_from_data (provider, btnCSSplusColor, 
                                                 -1, NULL);
                break;

            case 2:
                if(startBit == FALSE){
                    g_timeout_add_seconds(1, TimerFunc, timerLabel);
                    startBit = TRUE;
                }
                gtk_button_set_label(GTK_BUTTON(widget), val);
                sprintf(btnCSSplusColor, "%s%s", btnCSS, "color: #008000; }");
                gtk_css_provider_load_from_data (provider, btnCSSplusColor, 
                                                 -1, NULL);
                break;

            case 3:
                if(startBit == FALSE){
                    g_timeout_add_seconds(1, TimerFunc, timerLabel);
                    startBit = TRUE;
                }
                gtk_button_set_label(GTK_BUTTON(widget), val);
                sprintf(btnCSSplusColor, "%s%s", btnCSS, "color: #ff0000; }");
                gtk_css_provider_load_from_data (provider, btnCSSplusColor, 
                                                 -1, NULL);
                break;

            case 4:
                if(startBit == FALSE){
                    g_timeout_add_seconds(1, TimerFunc, timerLabel);
                    startBit = TRUE;
                }
                gtk_button_set_label(GTK_BUTTON(widget), val);
                sprintf(btnCSSplusColor, "%s%s", btnCSS, "color: #00008b; }");
                gtk_css_provider_load_from_data (provider, btnCSSplusColor, 
                                                 -1, NULL); 
                break;

            case 5:
                if(startBit == FALSE){
                    g_timeout_add_seconds(1, TimerFunc, timerLabel);
                    startBit = TRUE;
                }
                gtk_button_set_label(GTK_BUTTON(widget), val);
                sprintf(btnCSSplusColor, "%s%s", btnCSS, "color: #8b0000; }");
                gtk_css_provider_load_from_data (provider, btnCSSplusColor, 
                                                 -1, NULL);
                break;

            case 6:
                if(startBit == FALSE){
                    g_timeout_add_seconds(1, TimerFunc, timerLabel);
                    startBit = TRUE;
                }
                gtk_button_set_label(GTK_BUTTON(widget), val);
                sprintf(btnCSSplusColor, "%s%s", btnCSS, "color: #008080; }");
                gtk_css_provider_load_from_data (provider, btnCSSplusColor, 
                                                 -1, NULL);
                break;

            case 7:
                if(startBit == FALSE){
                    g_timeout_add_seconds(1, TimerFunc, timerLabel);
                    startBit = TRUE;
                }
                gtk_button_set_label(GTK_BUTTON(widget), val);
                sprintf(btnCSSplusColor, "%s%s", btnCSS, "color: #ffff00; }");
                gtk_css_provider_load_from_data (provider, btnCSSplusColor, 
                                                 -1, NULL);
                break;

            case 8:
                if(startBit == FALSE){
                    g_timeout_add_seconds(1, TimerFunc, timerLabel);
                    startBit = TRUE;
                }
                gtk_button_set_label(GTK_BUTTON(widget), val);
                sprintf(btnCSSplusColor, "%s%s", btnCSS, "color: #000000; }");
                gtk_css_provider_load_from_data (provider, btnCSSplusColor, 
                                                 -1, NULL);
                break;

            default:
                break;
        }
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(widget), TRUE);
        gtk_widget_set_sensitive(widget, FALSE);
    }
    GtkStyleContext *context = gtk_widget_get_style_context(widget);
    gtk_style_context_add_provider(context, GTK_STYLE_PROVIDER(provider), 
                                   GTK_STYLE_PROVIDER_PRIORITY_USER);
}

int main( int argc, char* argv[] ) {

    int i = 0,
        j = 0,
        n = -1,
        m = 0,
        max = 63,
        bombPositions[totalBombs],
        success = 0;

    srand(time(NULL));
    while(success != 1){
        for(i=0; i<totalBombs; i++){
            bombPositions[i] = rand()%max;
        }
        bSort(bombPositions, sizeof(int), totalBombs, intSort);
        success = arrayCheck(bombPositions, totalBombs);
    }

    for(i=0; i<8; i++){
        for(j=0; j<8; j++)
            field[i][j] = 0;
    }

    for(i=0; i<8; i++){
        for(j=0; j<8; j++){
            n++;
            for(m=0; m<totalBombs; m++)
                if(bombPositions[m] == n)
                    field[i][j] = -1;
        }
    }

    for(i=1; i<7; i++){
        for(j=1; j<7; j++){
            if(field[i][j] != -1){
                if(field[i-1][j-1] == -1) field[i][j]++;
                if(field[i-1][j]   == -1) field[i][j]++;
                if(field[i-1][j+1] == -1) field[i][j]++;
                if(field[i][j-1]   == -1) field[i][j]++;
                if(field[i][j+1]   == -1) field[i][j]++;
                if(field[i+1][j-1] == -1) field[i][j]++;
                if(field[i+1][j]   == -1) field[i][j]++;
                if(field[i+1][j+1] == -1) field[i][j]++;
            }
        }
    }

    i = 0;
    for(j=1; j<7; j++){
        if(field[i][j] != -1){
            if(field[i][j-1]   == -1) field[i][j]++;
            if(field[i][j+1]   == -1) field[i][j]++;
            if(field[i+1][j-1] == -1) field[i][j]++;
            if(field[i+1][j]   == -1) field[i][j]++;
            if(field[i+1][j+1] == -1) field[i][j]++;
        }
    }

    i = 7;
    for(j=1; j<7; j++){
        if(field[i][j] != -1){
            if(field[i-1][j-1] == -1) field[i][j]++;
            if(field[i-1][j]   == -1) field[i][j]++;
            if(field[i-1][j+1] == -1) field[i][j]++;
            if(field[i][j-1]   == -1) field[i][j]++;
            if(field[i][j+1]   == -1) field[i][j]++;
        }
    }

    j = 0;
    for(i=1; i<7; i++){
        if(field[i][j] != -1){
            if(field[i-1][j]   == -1) field[i][j]++;
            if(field[i-1][j+1] == -1) field[i][j]++;
            if(field[i][j+1]   == -1) field[i][j]++;
            if(field[i+1][j]   == -1) field[i][j]++;
            if(field[i+1][j+1] == -1) field[i][j]++;
        }
    }

    j = 7;
    for(i=1; i<7; i++){
        if(field[i][j] != -1){
            if(field[i-1][j]   == -1) field[i][j]++;
            if(field[i-1][j-1] == -1) field[i][j]++;
            if(field[i][j-1]   == -1) field[i][j]++;
            if(field[i+1][j-1] == -1) field[i][j]++;
            if(field[i+1][j]   == -1) field[i][j]++;
        }
    }

    if(field[0][0] != -1){
	    if(field[0][1] == -1) field[0][0]++;
	    if(field[1][0] == -1) field[0][0]++;
	    if(field[1][1] == -1) field[0][0]++;
    }

    if(field[0][7] != -1){
	    if(field[0][6] == -1) field[0][7]++;
	    if(field[1][6] == -1) field[0][7]++;
	    if(field[1][7] == -1) field[0][7]++;
    }

    if(field[7][0] != -1){
	    if(field[6][0] == -1) field[7][0]++;
	    if(field[6][1] == -1) field[7][0]++;
	    if(field[7][1] == -1) field[7][0]++;
    }

    if(field[7][7] != -1){
	    if(field[6][6] == -1) field[7][7]++;
	    if(field[6][7] == -1) field[7][7]++;
	    if(field[7][6] == -1) field[7][7]++;
    }

    score = 0;

    GtkWidget *window, *panel, *scoreLabel, *button, *closeButton;
    GtkCssProvider *provider0, *provider1, *provider2;
    GtkStyleContext *context;
    char id[3];

    gtk_init( &argc , &argv );

    builder = gtk_builder_new ();
    provider0 = gtk_css_provider_new();
    provider1 = gtk_css_provider_new();
    provider2 = gtk_css_provider_new();

    if( gtk_builder_add_from_string (builder, ui_xml, -1, &err) == 0 ) {
        printf("Error adding build from file. Error: %s\n", err->message);
    }

    timerLabel = GTK_WIDGET( gtk_builder_get_object (builder, "timer") );

    gtk_css_provider_load_from_data (provider0, "* { color: #000000; \
                                                     border-radius: 0px; \
                                                     background: #c0c0c0; \
                                                     border-color: #808080; \
                                                     border-width: 3px; \
                                                     border-style: outset; }",
                                     -1, NULL);

    gtk_css_provider_load_from_data (provider2, "* { color: #000000; \
                                                     border-radius: 0px; \
                                                     background: #c0c0c0; \
                                                     border-color: #808080; \
                                                     border-width: 0px; \
                                                     border-style: none; }",
                                     -1, NULL);

    window  = GTK_WIDGET( gtk_builder_get_object (builder, "window") );
    context = gtk_widget_get_style_context(window);
    gtk_style_context_add_provider(context, GTK_STYLE_PROVIDER(provider0), 
                                   GTK_STYLE_PROVIDER_PRIORITY_USER);

    mbtn = GTK_WIDGET( gtk_builder_get_object (builder, "mbtn") );
    context = gtk_widget_get_style_context(GTK_WIDGET(mbtn));
    gtk_style_context_add_provider(context, GTK_STYLE_PROVIDER(provider0), 
                                   GTK_STYLE_PROVIDER_PRIORITY_USER);

    xpmSmile = gdk_pixbuf_new_from_xpm_data((void *)smile_xpm);
    smile = (void *)gtk_image_new();
    gtk_image_set_from_pixbuf(smile, xpmSmile);
    gtk_button_set_image(GTK_BUTTON(mbtn), (void *)smile);

    closeButton = GTK_WIDGET( gtk_builder_get_object (builder, "closeButton") );
    context = gtk_widget_get_style_context(GTK_WIDGET(closeButton));
    gtk_style_context_add_provider(context, GTK_STYLE_PROVIDER(provider2), 
                                   GTK_STYLE_PROVIDER_PRIORITY_USER);

    xpmClose = gdk_pixbuf_new_from_xpm_data((void *)close_xpm);
    cloze = (void *)gtk_image_new();
    gtk_image_set_from_pixbuf(cloze, xpmClose);
    gtk_button_set_image(GTK_BUTTON(closeButton), (void *)cloze);

    panel = GTK_WIDGET( gtk_builder_get_object (builder, "panel") );
    context = gtk_widget_get_style_context(panel);
    gtk_style_context_add_provider(context, GTK_STYLE_PROVIDER(provider0), 
                                   GTK_STYLE_PROVIDER_PRIORITY_USER);

    for(int i=0; i<8; i++){
        for(int j=0; j<8; j++){
            sprintf(id, "%d%d", i, j);
            button = GTK_WIDGET( gtk_builder_get_object (builder, id) );
            context = gtk_widget_get_style_context(GTK_WIDGET(button));
            gtk_style_context_add_provider(context, 
                                           GTK_STYLE_PROVIDER(provider0), 
                                           GTK_STYLE_PROVIDER_PRIORITY_USER);
        }
    }

    gtk_css_provider_load_from_data (provider1, "* {border-radius: 0px; \
                                                    background: #1c1c1c; \
                                                    border-color: #808080; \
                                                    border-width: 3px; \
                                                    border-style: inset; \
                                                    padding: 5px 8px 2px 8px;}",
                                     -1, NULL);

    scoreLabel = GTK_WIDGET( gtk_builder_get_object (builder, "score") );
    context = gtk_widget_get_style_context(scoreLabel);
    gtk_style_context_add_provider(context, GTK_STYLE_PROVIDER(provider1), 
                                   GTK_STYLE_PROVIDER_PRIORITY_USER);

    timerLabel = GTK_WIDGET( gtk_builder_get_object (builder, "timer") );
    context = gtk_widget_get_style_context(timerLabel);
    gtk_style_context_add_provider(context, GTK_STYLE_PROVIDER(provider1), 
                                   GTK_STYLE_PROVIDER_PRIORITY_USER);

    gtk_builder_connect_signals (builder, NULL);

    gtk_widget_show_all( window );
    gtk_main();

    return 0;
}
