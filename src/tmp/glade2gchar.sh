#!/bin/bash

echo "const gchar *ui_xml =" > ui.h
cat $1\
| sed -e 's/  //g'\
| sed -e 's/"/\\"/g'\
| sed -e 's/^/"/g'\
| sed -e 's/$/"/g'\ >> ui.h
